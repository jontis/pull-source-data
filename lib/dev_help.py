import datetime

def date_str_tuple_to_date(date_tuple):
    return (datetime.datetime.strptime(date_tuple[0], '%Y-%m-%d').date(), datetime.datetime.strptime(date_tuple[1], '%Y-%m-%d').date())


def date_str_change_one(this_date, change):
    this_date = datetime.datetime.strptime(this_date, '%Y-%m-%d').date() + datetime.timedelta(days=change)
    return this_date.strftime('%Y-%m-%d')


def date_intervals_remaining(date_interval_local, date_interval_requested):
    local = date_str_tuple_to_date(date_interval_local)
    requested = date_str_tuple_to_date(date_interval_requested)
    if (requested[0] < local[0]) & (requested[1] < local[1]):
        return [(date_interval_requested[0], date_str_change_one(date_interval_local[0], -1))]
    elif (requested[0] < local[0]) & (requested[1] > local[1]):
        return [(date_interval_requested[0], date_str_change_one(date_interval_local[0], -1)), (date_str_change_one(date_interval_local[1], 1), date_interval_requested[1])]
    elif (requested[0] > local[0]) & (requested[1] > local[1]):
        return [(date_str_change_one(date_interval_local[1], 1), date_interval_requested[1])]
    else:
        return []

        
#hos to invoke interactive prompt, insert this line at the wanted position: interactive_python(locals())
def interactive_python(locals):
    import bpython   #change to #from IPython import embed# when IPython repo is available
    bpython.embed(locals_=locals, args=['-i', '-q'])

