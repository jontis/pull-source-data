import os
import urllib.parse as parse
import urllib.request
import json
from socket import timeout
from operator import itemgetter
import csv
import time
import dev_help

class SymbolFromYahoo:
    """symbol with dates and extra data to control download and updating"""
    ##TODO: add dates used for request, now only looks for available in local data
    ##TODO: create local data registry?
    def __init__(self, symbol, config):
        self.symbol = symbol
        self.config = config
        self.time_series = []
        self.file_name = self.symbol_to_file_name(symbol)
    
    
    def symbol_to_file_name(self, symbol):
        file_name = symbol if symbol[0] != '^' else 'i' + symbol[1:]
        file_name = file_name + '.' + self.config.local_storage['save_format']
        return file_name
    
    
    def local_data_available(self):
        if self.file_name in os.listdir(self.config.local_storage['save_folder']):
            return True
        else:
            return False


    def load_local_data(self):
        #print('read the local data', self.symbol)
        if self.config.local_storage['save_format'] == 'csv':
            csv_reader = csv.DictReader(open(self.config.local_storage['save_folder'] + self.file_name, 'r'), fieldnames = None, restval='', dialect='excel')
            self.time_series = [row for row in csv_reader]
            return (self.time_series[0]['Date'], self.time_series[-1]['Date'])
        
        
    def write_local_data(self):
        if len(self.time_series) > 0:
            if self.config.local_storage['save_format'] == 'csv':
                csv_writer = csv.DictWriter(open(self.config.local_storage['save_folder'] + self.file_name, 'w'), fieldnames = self.header(), restval='', dialect='excel')
                csv_writer.writeheader()
                csv_writer.writerows(self.time_series)
        else:
            print('time series empty, no data written', self.symbol)

    
    
    def download_data(self, date_interval_remaining):
        #chunk desired data, interval length, split in x year chunks to comply with yahoo processing limit, create holding object for data
        date_intervals = []
        for date_interval_i in date_interval_remaining:
            date_intervals.extend(self.chunker(date_interval_i, self.config.source['years_per_request']))

        data_set = []
        for date_interval_i in date_intervals:
            print('date interval: ', date_interval_i)
            data_set.extend(self.download_data_single_interval(date_interval_i))

        self.time_series.extend(data_set)
        if len(self.time_series) > 0: 
            self.time_series = sorted(self.time_series, key=itemgetter('Date'))
        else:
            print('Warning: empty data set for symbol ', self.symbol)


    def download_data_single_interval(self, date_interval):    
        #build url get string
        url_get_string = self.compose_request_url(date_interval)
    
        #fetch the data
        while(True):  #retry until successful download
            try:
                url_data = urllib.request.urlopen(url_get_string)
                data = url_data.read()
                #change response from bit type to actual type
                encoding = url_data.getheader('Content-type')
                if(encoding.find('utf-8') != -1): data = data.decode('utf-8')
                if(data.find('warning') > -1):
                    print('yahoo limit, wait 100 secs and retry')
                    time.sleep(self.config.source['sleep_on_limit_violation'])
                    raise ResourceWarning('yahoo limit')
                break
            except timeout:
                print('timeout')
            except ResourceWarning:
                print('yahoo limit')
   
        
        #handle empty response
        parsed_data = []
        #print('data rows read', json.loads(data)['query']['count'])
        if json.loads(data)['query']['count'] > 0:
            parsed_data = json.loads(data)['query']['results']['quote']
            if isinstance(parsed_data, dict): parsed_data = [parsed_data] #special case, make sure single item is returned as list as multiples are
        return parsed_data
    
        
    def header(self):
        self.column_names = self.time_series[0].keys()
        return self.column_names


    def chunker(self, interval, chunk_ys=1):
        start, stop = interval
        startYear = int(start.split('-')[0])
        stopYear = int(stop.split('-')[0])
        if startYear + chunk_ys -1 >= stopYear :
            return [interval]
        else:
            cropped_interval = [(interval[0], str(startYear + chunk_ys -1) + '-12-31')]
            rest_interval = (str(startYear + chunk_ys) + '-01-01', interval[1])
            cropped_interval.extend(self.chunker(rest_interval, chunk_ys))
            return cropped_interval
        
        
    def compose_request_url(self, date_interval):
        query_start = 'http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.historicaldata%20where%20symbol%20=%20%22'
        query_mid = '" and startDate = "' + date_interval[0] + '" and endDate = "' + date_interval[1]
        query_end = '"&format=json&diagnostics=true&env=store://datatables.org/alltableswithkeys'
        request_url = query_start + self.symbol + parse.quote(query_mid) + query_end
        return request_url
