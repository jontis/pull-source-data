#configure data sources
#yahoo seems to be limited in the length of requested data. chunk in yearly periods
import datetime
import os
startDate = '1980-01-01'
#startDate = '2010-01-01'
endDate = datetime.date.today().strftime('%Y-%m-%d') # '2012-06-05'
data_provider = 'yahoo_csv' #options: yahoo_csv / yahoo_yql / google
#why chose what? yahoo_yql seems to suffer from "tragedy of the commons", yahoo_csv has lately delivered one extra bad data row for as last row

#print('hej')
#print(os.getcwd())
if os.getcwd()[-3:] == 'bin':
    lib_path = os.getcwd()[:-3] + 'lib'
    root_path = os.getcwd()[:-3]

#when symbol list starts to get large, parse it from file
symbol_list = ['YHOO', '^DJI', '^GSPC', 'AXP', 'BA', 'BAC','CAT', 'CSCO', 'CVX', 'GE', 'HD', 'HPQ', 'IBM', 'INTC', 'JNJ', 'JPM', 'KO', 'MCD', 'MMM', 'MRK', 'MSFT', 'PFE', 'PG', 'T', 'VZ', 'WMT', 'XOM']
symbol_file = open(lib_path + '/config_symbols.txt')
symbol_list = list(map(lambda item: item.strip(), symbol_file.readlines()))
symbol_file.close()
#symbol_list = ['^GSPC']

local_storage = {
'save_folder': root_path + 'dataSource/',
'save_format': 'csv',
'compression': 'none'
}

source_yahoo_csv = {
'socket_timeout' : 30,
'max_concurrent_symbols' : 10,
'max_connections' : 10,
'years_per_request' : 50,
}

source_yahoo_yql = {
'socket_timeout' : 30,
'max_concurrent_symbols' : 2,
'max_connections' : 8,
'years_per_request' : 1,
'sleep_on_limit_violation' :100
}

source_google = {}

if data_provider == 'yahoo_csv': source = source_yahoo_csv
if data_provider == 'yahoo_yql': source = source_yahoo_yql
