def print_help():
    print('Usage: pullSourceData [OPTIONS]')
    print('')
    print('Downloads financial data from Yahoo api. Details are configured in lib/config if they are not given as command line options.')
    print('Default is to check which local data that is already available and complement it with remote data.')
    print('Data is saved in (default) folder dataSource/ in .csv format.')
    print('')
    print('Options:')
    print('  -O, --overwrite,  Deletes local data and downloads all requested data.')