#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 11 23:50:45 2018

@author: jonathan
"""

#https://www.cryptocompare.com/api/data/coinlist/
#https://min-api.cryptocompare.com/data/all/coinlist


import os
import re
import pandas as pd
import urllib.request, json
from datetime import datetime
import sklearn.model_selection
import time

class CFG:
    pass

cfg = CFG
cfg.data_path_base = '/home/jonathan/darkHorseInvestments/'
cfg.data_path_gen =  cfg.data_path_base + 'dataGenerated/'
cfg.data_path_source = cfg.data_path_base + 'dataSource/'
cfg.data_path_source_coins = cfg.data_path_source + 'coins/'

cfg.min_time_span = 30
cfg.redownload = False

# load total carket cap for all coins
# scraped from this page https://coinmarketcap.com/charts/
df_total_market_cap_raw = pd.read_json('https://graphs2.coinmarketcap.com/global/marketcap-total/')
def My_split_1(in_list):
    return in_list[1]
df_total_market_cap = pd.DataFrame(list(df_total_market_cap_raw.market_cap_by_available_supply.values))
df_total_market_cap.columns = ['timestamp', 'market_cap']
df_total_market_cap['traded_volume'] = df_total_market_cap_raw.volume_usd.apply(My_split_1)
df_total_market_cap['date'] = pd.to_datetime(df_total_market_cap.timestamp, unit='ms').apply(datetime.date)
df_total_market_cap.drop_duplicates(subset='date', inplace=True)
df_total_market_cap['market_cap_diff'] = df_total_market_cap.market_cap.diff()
df_total_market_cap['logmarket_cap'] = df_total_market_cap.market_cap.apply(pd.np.log)
df_total_market_cap['logret'] = df_total_market_cap.logmarket_cap.diff()
df_total_market_cap.set_index('date', inplace=True)
df_total_market_cap.to_csv(cfg.data_path_source_coins + 'df_total_market_cap.csv', index=False)



df_coinmarketcap = pd.read_json('https://api.coinmarketcap.com/v1/ticker/')

# call for hourly historical data
# example https://min-api.cryptocompare.com/data/histohour?fsym=BTC&tsym=USD&limit=60&aggregate=3&e=CCCAGG

# limited to 2000 hours, ~83 days, for hourly data 

# hourly data
#list_df_coin = []
#with urllib.request.urlopen('https://min-api.cryptocompare.com/data/histohour?fsym=BTC&tsym=USD&limit=2000&aggregate=1') as url:
#    data = json.loads(url.read().decode())
#    df_coin_data = pd.DataFrame(data['Data'])
#    list_df_coin.append(df_coin_data)



def Download_data(list_coin_symbols = None):
    # if you supply list of coin symbols, this only downloads them
    # daily data
    #TODO: if you got the list_coin_symbols argument, translate the strange symbols like 'star' ...
    def Download_coin(i_sym):
        with urllib.request.urlopen('https://min-api.cryptocompare.com/data/histoday?fsym=' + i_sym + '&tsym=USD&allData=true&aggregate=1') as url:
            data = json.loads(url.read().decode())
            df_coin_data = pd.DataFrame(data['Data'])
            return df_coin_data
        
    if list_coin_symbols is None:
        df_coins_data = pd.read_json('https://min-api.cryptocompare.com/data/all/coinlist')
        df_coins = pd.concat([pd.Series(df_coins_data.iloc[i,:].Data) for i in range(df_coins_data.shape[0])], axis=1, ignore_index=True).transpose()        
        
        # clean up column TotalCoinSupply from bad chars
        df_coins.TotalCoinSupply = df_coins.TotalCoinSupply.apply(lambda x: re.sub("[^0-9]", "", str(x)))
        df_coins.loc[df_coins.TotalCoinSupply == '', 'TotalCoinSupply'] = 0
        df_coins.TotalCoinSupply = df_coins.TotalCoinSupply.astype('float')
    
        list_coin_symbols = df_coins.Symbol.values.tolist()
        list_coin_symbols = [x for x in list_coin_symbols if type(x) is str]
    
    dict_df_coin = {}
    for i_sym in list_coin_symbols:
        print(i_sym)
        # rate limit?
        df_coin_data = pd.DataFrame()        
        # sanitize names from '*'
        if i_sym.find('*') != -1:
            i_sym = i_sym.replace('*', 'star')
        df_coin_data = Download_coin(i_sym)
        # if you got a zero reply, wait 10 sec and try again
        if df_coin_data.shape[0] < 1:
            time.sleep(1)
            df_coin_data = Download_coin(i_sym)

        dict_df_coin.update({i_sym: df_coin_data})
    
    # persist the source data to minimize redownloading
    for i_sym in list(dict_df_coin.keys()):
        dict_df_coin[i_sym].to_csv(cfg.data_path_source_coins + i_sym + '.csv', index=False)


def Feature_create(in_df_coin):  
    df_temp = in_df_coin.copy()
    df_temp['datetime'] = df_temp.time.apply(datetime.fromtimestamp)
    df_temp['date'] = df_temp.datetime.apply(datetime.date)
    df_temp['close_diff'] = df_temp.close.diff()
    df_temp['logclose'] = df_temp.close.apply(pd.np.log)
    df_temp['logret'] = df_temp.logclose.diff()
    df_temp.drop_duplicates(subset='date', inplace=True)
    df_temp.set_index('date', inplace=True)
    df_temp['logretcorr'] = df_temp['logret'] - df_total_market_cap[df_total_market_cap.index.isin(df_temp.index)]['logret']
    return df_temp


# load BTC data if present in source dir and cfg.redownload is not set
if os.path.exists(cfg.data_path_source_coins + 'BTC.csv') & (not cfg.redownload):
    print('coin data loading from disk')
    df_coins = pd.read_csv(cfg.data_path_source + 'df_coins.csv')
    dict_df_coin = {}
    for i_file in os.listdir(cfg.data_path_source_coins):
        # not the 1 bytes
        if 1 < os.path.getsize(cfg.data_path_source_coins + i_file):       
            df_coin_data = pd.read_csv(cfg.data_path_source_coins + i_file)
            i_sym = i_file.split('.')[0]
            dict_df_coin.update({i_sym: df_coin_data})
else:
    print('coin data downloading')
    Download_data()
    # check which ones got 1 byte, rate limit?
    list_files = os.listdir(cfg.data_path_source_coins)
    list_symbols_retry_download = [x.split('.')[0] for x in list_files if os.path.getsize(cfg.data_path_source_coins + x) == 1]
    Download_data(list_symbols_retry_download)
    # try downloading again
        

# clean data frames with less than cfg.min_time_span
for i_sym in list(dict_df_coin.keys()):
    if dict_df_coin[i_sym].shape[0] < cfg.min_time_span:
        dict_df_coin.pop(i_sym)
 

# feature creation
for i_sym in list(dict_df_coin.keys()):
    dict_df_coin.update({i_sym: Feature_create(dict_df_coin[i_sym])})
    

# create matrix of close prices for all coins
list_df_close = []
for i_sym in list(dict_df_coin.keys()):
    df_temp = dict_df_coin[i_sym].close
    df_temp.name = i_sym
    list_df_close.append(df_temp)
df_close = pd.concat(list_df_close, axis=1)
# drop last row if it's not "complete"
while 0 < df_close.iloc[-1].isnull().sum():
    print('dropped last row')
    df_close = df_close.iloc[:-1].copy()
df_close.fillna(0, inplace=True)

# create total market cap matrix
df_total_coin_supply = df_coins[['Symbol', 'TotalCoinSupply']].copy()
df_total_coin_supply.dropna(inplace=True)
df_total_coin_supply.set_index('Symbol', inplace=True)
list_coins = [i_sym for i_sym in list(df_close.columns) if i_sym in list(df_total_coin_supply.index)]
list_drop_cols = [i_sym for i_sym in list(df_close.columns) if i_sym not in list_coins]

#df_full_market_cap = df_close.copy()
#df_full_market_cap.drop(list_drop_cols, axis=1, inplace=True)
#for i_sym in list(df_full_market_cap.columns):
#    df_full_market_cap.loc[:, i_sym] = df_full_market_cap.loc[:, i_sym] * float(df_total_coin_supply.loc[i_sym][0])
#
#df_relative_market_cap = df_full_market_cap.copy()
#df_relative_market_cap = df_relative_market_cap.df_full_market_cap.sum(axis=1)

# try to build first generator for training sets.
# base on 250 previous logretcorr
# target: next 10 logretcorr
# extra features? marketcap, weekday
# forest
# from 2015 - 
# step 30 days

df_dict_symbols = pd.DataFrame(list(dict_df_coin.keys()))
# rolling window prediction

# build features and target

# extract time vectors of feature + target length

# take one symbol at a time

df_coin = dict_df_coin[i_sym]
date_start = pd.datetime(2015, 1, 1).date()


# separate target    
y_ml = df_ml.failing_part_no
#y_test = df_ml.failing_part_no
X_ml = df_ml.drop(['failing_part_no', 'failing_part_filtered'], axis=1)
#X_test = df_ml.drop('failing_part_no', axis=1)
