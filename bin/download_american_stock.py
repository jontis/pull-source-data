#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 27 02:19:49 2017

@author: jonathan
"""

import pandas as pd


demo_url = 'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=MSFT&apikey=demo&datatype=csv'

"""TIME_SERIES_DAILY High Usage
This API returns daily time series (date, daily open, daily high, daily low, daily close, daily volume) of the equity specified, covering up to 20 years of historical data.
The most recent data point is the cumulative prices and volume information of the current trading day, updated realtime.
API Parameters
❚ Required: function
The time series of your choice. In this case, function=TIME_SERIES_DAILY
❚ Required: symbol
The name of the equity of your choice. For example: symbol=MSFT
❚ Optional: outputsize
By default, outputsize=compact. Strings compact and full are accepted with the following specifications: compact returns only the latest 100 data points; full returns the full-length time series of up to 20 years of historical data. The "compact" option is recommended if you would like to reduce the data size of each API call.
❚ Optional: datatype
By default, datatype=json. Strings json and csv are accepted with the following specifications: json returns the daily time series in JSON format; csv returns the time series as a CSV (comma separated value) file.
❚ Required: apikey
"""

class CFG:
    pass

cfg = CFG
cfg.data_path_base = '/home/jonathan/darkHorseInvestments/'
cfg.data_path_gen =  cfg.data_path_base + 'dataGenerated/'
cfg.data_path_source = cfg.data_path_base + 'dataSource/'
cfg.apikey = 'G7R1ATR6W2UXH6R0' # get api key from their homepage if this stops working

string_symbol = 'YHOO ^DJI ^GSPC AXP BA BAC CAT CSCO CVX GE HD HPQ IBM INTC JNJ JPM KO MCD MMM MRK MSFT PFE PG T VZ WMT XOM ^JKSE ^HSI ^OMX MCHI EEM EWZ FXI'
list_symbol = string_symbol.split()

def BuildUrl(symbol):
    return 'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=' + symbol + '&outputsize=full&apikey=' + cfg.apikey + '&datatype=csv'

symbol = 'MSFT'
for symbol in list_symbol:
   print(symbol)
   data_url = BuildUrl(symbol)
   df_data = pd.read_csv(data_url)
   if 99 < df_data.shape[0]:
       df_data.to_csv(cfg.data_path_source + 'stock_' + symbol + '.csv', index=False)
   else:
       print('no data received for symbol: ' + symbol)
       