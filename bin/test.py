#! /usr/bin/env python3.2

#purpose of this script: download historical financial data from google / yahoo and store it in local files for import to R
#download all of symbols listed in config from start date until today or if any data is already available, append
#option --overwrite discards all local data and downloads again

#TODO: autoupdate all symbols in local storage
#TODO: read option with symbols from argv?
#TODO: find database format to store in and read from R
#TODO: compression?
#TODO: implement google as data provider
#TODO: fetch list of available symbols and available data for each of them?
#TODO: write registry of written data files?

#import core libraries
import os
import sys
import getopt
import socket
import threading
import concurrent.futures

#import urllib.response
#import xml.dom
import gzip

#add this scripts lib path to module search path
sys.path.insert(1,os.path.join(os.path.dirname(sys.path[0]),'lib'))

#parse command line options
options, remainder = getopt.gnu_getopt(sys.argv[1:], 'h:O', ['help', 'overwrite'])
update = True
for opt, arg in options:
    if opt in ('-O', '--overwrite'):
        update = False
    elif opt in ('-h', '--help'):
        import print_help
        print_help.print_help()
        sys.exit()
        
print(os.getcwd())      # Return the current working directory



#read config
import config
import symbol_from_yahoo_debug

#override for test, only one thread
config.max_concurrent_symbols = 1
config.max_connections = 1

#import extra libraries
import dev_help

#timeout for each data request
socket.setdefaulttimeout(config.source['socket_timeout'])

#timeout for each data request
socket.setdefaulttimeout(config.source['socket_timeout'])

for this_symbol in config.symbol_list:
    #print('processing ' + this_symbol)
    symbol = symbol_from_yahoo_debug.SymbolFromYahoo(this_symbol, config)
    if update & symbol.local_data_available():
        date_interval_local = symbol.load_local_data()
        date_intervals_remaining = dev_help.date_intervals_remaining(date_interval_local, (config.startDate, config.endDate))
        symbol.download_data(date_intervals_remaining)
    else:
        #print('full download', this_symbol)                
        symbol.download_data([(config.startDate, config.endDate)])
        
    symbol.write_local_data()
    print('finished ' + this_symbol)
        

    

    
    #dev_help.interactive_python(locals())
    
    


