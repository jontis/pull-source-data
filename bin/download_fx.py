#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 27 02:19:49 2017

@author: jonathan
"""

import pandas as pd


demo_url = 'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=USD&to_currency=JPY&apikey=demo'

"""Realtime currency exchange rates for physical and digital currencies.
CURRENCY_EXCHANGE_RATE High Usage
This API returns the realtime exchange rate for any pair of digital currency (e.g., Bitcoin) or physical currency (e.g., USD).
API Parameters
❚ Required: function
The function of your choice. In this case, function=CURRENCY_EXCHANGE_RATE
❚ Required: from_currency
The currency you would like to get the exchange rate for. It can either be a physical currency or digital/crypto currency. For example: from_currency=USD or from_currency=BTC.
❚ Required: to_currency
The destination currency for the exchange rate. It can either be a physical currency or digital/crypto currency. For example: to_currency=USD or to_currency=BTC.
❚ Required: apikey
"""

class CFG:
    pass

cfg = CFG
cfg.data_path_base = '/home/jonathan/darkHorseInvestments/'
cfg.data_path_gen =  cfg.data_path_base + 'dataGenerated/'
cfg.data_path_source = cfg.data_path_base + 'dataSource/'
cfg.apikey = 'G7R1ATR6W2UXH6R0' # get api key from their homepage if this stops working

base_currency = 'USD'
string_symbol = 'ARS BRL CAD EUR GBP HKD JPY BTC DASH ETH DOGE LTC OMNI XMR XRP'
list_symbol = string_symbol.split()

def BuildUrl(symbol):
    return 'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=' + symbol + '&to_currency=' + base_currency + '&apikey=' + cfg.apikey
    #return 'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=' + symbol + '&outputsize=full&apikey=' + cfg.apikey + '&datatype=csv'

symbol = 'EUR'
for symbol in list_symbol:
   print(symbol)
   data_url = BuildUrl(symbol)
   df_data = pd.read_csv(data_url)
   if 99 < df_data.shape[0]:
       df_data.to_csv(cfg.data_path_source + symbol + '.csv', index=False)
   else:
       print('no data received for symbol: ' + symbol)
       