#! /usr/bin/env python3.2

#purpose of this script: download historical financial data from google / yahoo and store it in local files for import to R
#download all of symbols listed in config from start date until today or if any data is already available, append
#option --overwrite discards all local data and downloads again

#TODO: impose limits: don't call YQL more than 0.2 times/second or 1,000 times/hour for IP
#TODO: autoupdate all symbols in local storage
#TODO: read option with symbols from argv?
#TODO: find database format to store in and read from R
#TODO: compression?
#TODO: implement google as data provider
#TODO: fetch list of available symbols and available data for each of them?
#TODO: write registry of written data files?
#TODO: persist data in other format? database? pickle?

#import core libraries
import os
import sys
import getopt
import socket
import threading
import concurrent.futures


#add this scripts lib path to module search path
sys.path.insert(1,os.path.join(os.path.dirname(sys.path[0]),'lib'))
lib_path = os.getcwd().strip('bin') + 'lib'
sys.path.append(lib_path)

#parse command line options
options, remainder = getopt.gnu_getopt(sys.argv[1:], 'h:O', ['help', 'overwrite'])
update = True
for opt, arg in options:
    if opt in ('-O', '--overwrite'):
        update = False
    elif opt in ('-h', '--help'):
        import print_help
        print_help.print_help()
        sys.exit()
        
print(os.getcwd())      # Return the current working directory



#read config
import config
import symbol_classes

#import extra libraries
import dev_help

#timeout for each data request
socket.setdefaulttimeout(config.source['socket_timeout'])

#multi thread
thread_pool_semaphore = threading.BoundedSemaphore(value=config.source['max_concurrent_symbols'])
executor_connection = concurrent.futures.ThreadPoolExecutor(max_workers=config.source['max_connections'])

def worker(this_symbol):
    with thread_pool_semaphore:
        try:
            print('processing ' + this_symbol)
            symbol = symbol_classes.SymbolFromYahooCsv(this_symbol, config)  #need switch here to select class from config
            if update & symbol.local_data_available():
                date_interval_local = symbol.load_local_data()
                date_intervals_remaining = dev_help.date_intervals_remaining(date_interval_local, (config.startDate, config.endDate))
                symbol.download_data(date_intervals_remaining, executor_connection)
            else:
                print('full download ' + this_symbol)
                symbol.download_data([(config.startDate, config.endDate)], executor_connection)
            
            symbol.write_local_data()
        finally:
            print('finished ' + this_symbol)
    return

threads = []
for symbol in config.symbol_list:
    t = threading.Thread(target=worker, args=[symbol])
    threads.append(t)
    t.start()
    
for t in threads: t.join()
        

    

    
    


