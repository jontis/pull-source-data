#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 11 23:50:45 2018

@author: jonathan
"""

#https://www.cryptocompare.com/api/data/coinlist/
#https://min-api.cryptocompare.com/data/all/coinlist

min_time_span = 30

import pandas as pd
import urllib.request, json
from datetime import datetime

class CFG:
    pass

cfg = CFG
cfg.data_path_base = '/home/jonathan/darkHorseInvestments/'
cfg.data_path_gen =  cfg.data_path_base + 'dataGenerated/'
cfg.data_path_source = cfg.data_path_base + 'dataSource/'

df_coins_data = pd.read_json('https://min-api.cryptocompare.com/data/all/coinlist')

df_coins = pd.concat([pd.Series(df_coin_data.iloc[i,:].Data) for i in range(df_coin_data.shape[0])], axis=1).transpose()


# call for hourly historical data
# example https://min-api.cryptocompare.com/data/histohour?fsym=BTC&tsym=USD&limit=60&aggregate=3&e=CCCAGG

# limited to 2000 hours, ~83 days, for hourly data 

# hourly data
#list_df_coin = []
#with urllib.request.urlopen('https://min-api.cryptocompare.com/data/histohour?fsym=BTC&tsym=USD&limit=2000&aggregate=1') as url:
#    data = json.loads(url.read().decode())
#    df_coin_data = pd.DataFrame(data['Data'])
#    list_df_coin.append(df_coin_data)

# daily data
list_coin_symbols = df_coins.Symbol.values.tolist()
list_coin_symbols = [x for x in list_coin_symbols if type(x) is str]
dict_df_coin = {}
for i_sym in list_coin_symbols:
    print(i_sym)
    with urllib.request.urlopen('https://min-api.cryptocompare.com/data/histoday?fsym=' + i_sym + '&tsym=USD&allData=true&aggregate=1') as url:
        data = json.loads(url.read().decode())
        df_coin_data = pd.DataFrame(data['Data'])
        # sanitize names from '*'
        if i_sym.find('*') != -1:
            i_sym = i_sym.replace('*', 'star')
        dict_df_coin.update({i_sym: df_coin_data})


        

# persist the source data to minimize redownloading
for i_sym in list(dict_df_coin.keys()):
    dict_df_coin[i_sym].to_csv(cfg.data_path_source + i_sym + '.csv', index=False)


def Feature_create(in_df_coin):  
    df_temp = in_df_coin.copy()
    df_temp['datetime'] = df_temp.time.apply(datetime.fromtimestamp)
    df_temp['close_diff'] = df_temp.close.diff()
    df_temp['logclose'] = df_temp.close.apply(log)
    df_temp['logret'] = df_temp.logclose.diff()
    return df_temp

# clean data frames with less than cfg.min_time_span
for i_sym in list(dict_df_coin.keys()):
    if dict_df_coin[i_sym].shape[0] < min_time_span:
        dict_df_coin.pop(i_sym)
    
# feature creation
for i_sym in list(dict_df_coin.keys()):
    dict_df_coin.update({i_sym: Feature_create(dict_df_coin[i_sym])})
    
    
exp(aaa.logret[-10:].sum())

# process data
time_span_aggregate = 

# join daily data